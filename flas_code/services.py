from flask import render_template,request,redirect
from Appfactory.plugin import db
from Appfactory.Models import StudentModel,StaffModel
from blueprints import index

@index.route('/create/student' , methods = ['GET','POST'])
def create():
    if request.method == 'GET':
        return render_template('create_student.html')

    if request.method == 'POST':
        studid = request.form['studid']
        stdname = request.form['stdname']
        department = request.form['department']
        year = request.form['year']

        student = StudentModel()
        db.session.add(student)
        db.session.commit()
        return redirect('/data')


@index.route('/creating/staff' , methods = ['GET','POST'])
def createfun():
    if request.method == 'GET':
        return render_template('Create_staff.html')

    if request.method == 'POST':
        staffid = request.form['staffid']
        staffname = request.form['staffname']
        department = request.form['department']
        position = request.form['position']
        staff = StaffModel(staffid=staffid,staffname=staffname,department=department,position=position)
        db.session.add(staff)
        db.session.commit()
        return redirect('/datastaff')

@index.route('/data' , methods = ['GET','POST'])
def createstudent1():
    return render_template('create_student.html')

@index.route('/datastaff' , methods = ['GET','POST'])
def createstaff():
    return render_template('data_staff1.html')


@index.route('/staff', methods = ['GET','POST'])
def RetrieveList():
    return render_template('datastaff.html',staff = staff)
