from flask import Flask,render_template,request,redirect,jsonify
from flask_sqlalchemy import SQLAlchemy


import pymysql
pymysql.install_as_MySQLdb()


app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://root:password@localhost/data'
db=SQLAlchemy(app)

class EmployeeModel(db.Model):

    id = db.Column(db.Integer, primary_key=True)
    employee_id = db.Column(db.Integer(),unique = True)
    name = db.Column(db.String(100))
    age = db.Column(db.Integer())
    position = db.Column(db.String(80))

db.create_all()


@app.route('/')
def index():
    return 'WELCOME TO DORUSTREE!!!'

 # Create
@app.route('/data/create' , methods = ['GET','POST'])
def create():
    if request.method == 'GET':
        return render_template('create.html')

    if request.method == 'POST':
        employee_id = request.form['employee_id']
        name = request.form['name']
        age = request.form['age']
        position = request.form['position']
        employee = EmployeeModel(employee_id=employee_id, name=name, age=age, position = position)
        db.session.add(employee)
        db.session.commit()
        return redirect('/data')

 # Retrive

@app.route('/data')
def RetrieveList():
    # employees = EmployeeModel.query.all()
    # data=[]
    # for employee in employees:
    #     x = employee.__dict__
    #     data.append(x)
    #     del x["_sa_instance_state"]

    # return jsonify(data)


    return render_template('datalist.html',employees = employees)


@app.route('/data/<int:id>')
def RetrieveEmployee(id):
    employee = EmployeeModel.query.filter_by(employee_id=id).first()
    if employee:
        return render_template('data_stud.html', employee = employee)
    return "Employee with id ={id} does not exist"


 #Update

@app.route('/data/<int:id>/update',methods = ['GET','POST'])
def update(id):
    employee = EmployeeModel.query.filter_by(employee_id=id).first()
    if request.method == 'POST':
        if employee:
            db.session.delete(employee)
            db.session.commit()
            name = request.form['name']
            age = request.form['age']
            position = request.form['position']
            employee = EmployeeModel(employee_id=id, name=name, age=age, position = position)
            db.session.add(employee)
            db.session.commit()
            return redirect(f'/data/{id}')
        return f"Employee with id = {id} Does not exist"

    return render_template('update.html', employee = employee)


 # Delete
@app.route('/data/<int:id>/delete', methods=['GET','POST'])
def delete(id):
    employee = EmployeeModel.query.filter_by(employee_id=id).first()
    if request.method == 'POST':
        if employee:
            db.session.delete(employee)
            db.session.commit()
            return redirect('/data')
        
    return render_template('delete.html')

if __name__ == "__main__":
    app.run(debug=True,port=4000)
