from Appfactory.plugin import db

class StudentModel(db.Model):

    id = db.Column(db.Integer, primary_key=True)
    studid = db.Column(db.Integer(),unique = True)
    stdname = db.Column(db.String(100))
    department= db.Column(db.String(100))
    year = db.Column(db.Integer())
    staff_id=db.Column(db.Integer, db.ForeignKey('staff_model.staffid'),nullable=False)
    staff = db.relationship('StaffModel', backref='staff')



class StaffModel(db.Model):

    id = db.Column(db.Integer, primary_key=True)
    staffid = db.Column(db.Integer(),unique = True)
    staffname = db.Column(db.String(100))
    department= db.Column(db.String(100))
    position = db.Column(db.String(50))








