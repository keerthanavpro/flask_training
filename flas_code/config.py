class Config(object):

   SQLALCHEMY_COMMIT_ON_TEARDOWN = True
   SQLALCHEMY_TRACK_MODIFICATIONS = False

class development(Config):
   ENV='development'
   DEBUG = True
   TESTING = False
   SQLALCHEMY_DATABASE_URI = 'mysql://root:password@localhost/join_tables'

