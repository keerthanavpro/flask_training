from flask import Flask
from flask_sqlalchemy import SQLAlchemy

import pymysql

import Appfactory.config

pymysql.install_as_MySQLdb()
db=SQLAlchemy()


def modelcal():
    app = Flask(__name__)
    app.config.from_object("Appfactory.config.development")
    db.__init__(app)
    with app.app_context():
        from Appfactory import services
        app.register_blueprint(services.index)
        db.create_all()
    return app

